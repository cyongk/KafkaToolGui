package cn.cyk.kafkatoolgui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(Main.class.getResource("/cn/cyk/kafkatoolgui/view/kafka-produce.fxml"));
        primaryStage.setTitle("Kafka生产消息");
        primaryStage.setScene(new Scene(root, 690, 480));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
